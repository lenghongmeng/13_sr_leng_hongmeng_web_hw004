import { Component } from "react";
import Swal from "sweetalert2";

export default class TableComponent extends Component {

    handlePopup = (list) => {
        Swal.fire({
            html: ' <p style="font-size: 24px; font-weight: bold"> Information </p> <br> <hr>  <br> <p style="width: 100%; line-height: 30px"> ID: ' + list.id + '</br>Email: ' + list.email + '</br>Username: ' + list.username + '</br>Age: ' + list.age + '</p>',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK'
        });
    }

    render() {
        return (
            <>
                <div className="flex min-h-full items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
                    <div className="w-full max-w-4xl space-y-8">
                        <div className="relative overflow-x-auto">
                            <table className="w-full text-sm text-left">
                                <thead className="text-xs uppercase bg-gray-50 text-center">
                                    <tr>
                                        <th scope="col" className="px-6 py-3">
                                            ID
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            Email
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            Username
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            Age
                                        </th>
                                        <th scope="col" className="px-6 py-3">
                                            Action
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.props.data.length != 0 ? this.props.data.map((list, index) => (
                                        <tr className={`text-center border-b ${(index + 1) % 2 == 0 ? "bg-pink-100" : "bg-white"}`}>
                                            <td scope="row" className="px-6 py-4">
                                                {list.id}
                                            </td>
                                            <td scope="row" className="px-6 py-4">
                                                {list.email == "" ? "Null" : list.email}
                                            </td>
                                            <td className="px-6 py-4">
                                                {list.username == "" ? "Null" : list.username}
                                            </td>
                                            <td className="px-6 py-4">
                                                {list.age == "" ? "Null" : list.age}
                                            </td>
                                            <td className="px-6 py-4">
                                                <button onClick={() => this.props.handleStatusChange(list)} className={`rounded-md px-6 py-1 text-white mx-2 my-1 ${list.status == "Pending" ? "bg-red-700" : "bg-green-700"}`}>{list.status}</button>
                                                <button onClick={() => this.handlePopup(list)} className="rounded-md bg-blue-700 px-6 py-1 text-white mx-2 my-1">Show more</button>
                                            </td>
                                        </tr>
                                    )) :
                                        <tr className="bg-white  border-b">
                                            <td colSpan={5} className="px-6 py-4 text-center font-bold">
                                                No Data
                                            </td>
                                        </tr>
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}