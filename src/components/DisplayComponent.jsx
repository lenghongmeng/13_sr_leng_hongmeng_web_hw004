import { Component, useState } from "react";
import TableComponent from "./TableComponent";

export default class DisplayComponent extends Component {

    constructor() {
        super();
        this.state = {
            data: [
                // { id: 1, email: "meng@gmail.com", username: "hongmeng", age: 25, status: "Pending"},
                // { id: 2, email: "hong@gmail.com", username: "hong", age: 23, status: "Pending"}
            ],
            getId: null,
            getEmail: "",
            getUsername: "",
            getAge: "",
            getStatus: "Pending",

            getMsgEmail: "",
            getMsgUsername: "",
            getMsgAge: "",
        }
    }

    handleStatusChange = (e) => {
        this.state.data.map(ele => {
            if (e.id == ele.id) {
                ele.status = ele.status == "Pending" ? "Done" : "Pending"
            }
        })

        this.setState({
            data: this.state.data
        })
    }

    handleInputEmail = (e) => {
        this.setState({
            getEmail: e.target.value,
        })
    }

    handleInputUsername = (e) => {
        this.setState({
            getUsername: e.target.value,
        })
    }

    handleInputAge = (e) => {
        this.setState({
            getAge: e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();

        let patternEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
        let patternUsername = /^[A-Za-z][A-Za-z0-9_]{7,29}$/
        let patternAge = /^[1-9]?[0-9]{1}$|^120$/

        var getEmail = this.state.getEmail;
        var getUsername = this.state.getUsername;
        var getAge = this.state.getAge;

        var getMsgEmail = "";
        var getMsgUsername = "";
        var getMsgAge = "";

        var isValidEmail = false
        var isValidUsername = false
        var isValidAge = false


        if(getEmail === "") {
            getMsgEmail = "Email address cannot be null"
            isValidEmail = false
        }
        else if(getEmail.match(patternEmail) == null) {
            getMsgEmail = "Invalid email address"
            isValidEmail = false
        }
        else {
            getMsgEmail = ""
            isValidEmail = true
        }

        if(getUsername === "") {
            getMsgUsername = "Username cannot be null"
            isValidUsername = false
        }
        else if(getUsername.match(patternUsername) == null) {
            getMsgUsername = "Username must be more than 8 character and start with letter"
            isValidUsername = false
        } 
        else {
            getMsgUsername = ""
            isValidUsername = true
        }

        if(getAge === "") {
            getMsgAge = "Age cannot be null"
            isValidAge = false
        }
        else if(getAge.match(patternAge) == null) {
            getMsgAge = "Age must be positive number, less than 120 and not floating number"
            isValidAge = false
        } 
        else {
            getMsgAge = ""
            isValidAge = true
        }

        if(isValidEmail && isValidUsername && isValidAge) {
            const newData = {
                id: this.state.data.length + 1,
                email: this.state.getEmail,
                username: this.state.getUsername,
                age: this.state.getAge,
                status: this.state.getStatus
            }
    
            this.setState({
                data: [...this.state.data, newData],
                getMsgEmail: "",
                getMsgUsername: "",
                getMsgAge: "",
                getEmail: "",
                getUsername: "",
                getAge: "",
            })
            
        } else {
            this.setState({
                getMsgEmail,
                getMsgUsername,
                getMsgAge
            })
        }
    }

    render() {
        return (
            <>
                <div className="flex min-h-full items-center justify-center pt-12 px-4 mb-6">
                    <div className="w-full max-w-lg">
                        <p className="text-4xl font-extrabold">Please Fill Your Information</p>
                    </div>
                </div>
                <div class="max-w-lg m-auto p-6  rounded-md shadow">
                    <form className="mt-8 space-y-2">
                        <label className="block mb-2 text-md font-bold">Email</label>
                        <div className="flex mb-4">
                            <span className="inline-flex items-center px-3 text-sm border border-r-0 border-gray-300 rounded-l-md">
                                <svg aria-hidden="true" className="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path><path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path></svg>
                            </span>
                            <input type="email" onChange={this.handleInputEmail} name="email" className="rounded-none rounded-r-lg border block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5 dark:focus:ring-blue-500" placeholder="name@gmail.com" value={this.state.getEmail}/>
                        </div>
                        <p class="mt-2 text-sm text-red-600 dark:text-red-500">{this.state.getMsgEmail}</p>
                        <div className="py-1"></div>
                        <label className="block mb-2 text-md font-bold">Username</label>
                        <div className="flex mb-4">
                            <span className="inline-flex items-center px-3 text-sm border border-r-0 border-gray-300 rounded-l-md">
                                @
                            </span>
                            <input type="text" onChange={this.handleInputUsername} name="username" className="rounded-none rounded-r-lg border block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5 dark:focus:ring-blue-500" placeholder="Name" value={this.state.getUsername}/>
                        </div>
                        <p class="mt-2 text-sm text-red-600 dark:text-red-500">{this.state.getMsgUsername}</p>
                        <div className="py-1"></div>
                        <label className="block mb-2 text-md font-bold">Age</label>
                        <div className="flex mb-4">
                            <span className="inline-flex items-center px-3 text-sm border border-r-0 border-gray-300 rounded-l-md">
                                💜
                            </span>
                            <input type="number" onChange={this.handleInputAge} className="rounded-none rounded-r-lg border block flex-1 min-w-0 w-full text-sm border-gray-300 p-2.5 dark:focus:ring-blue-500" placeholder="Age" value={this.state.getAge}/>
                        </div>
                        <p class="mt-2 text-sm text-red-600 dark:text-red-500">{this.state.getMsgAge}</p>

                        <br/>
                        <div className="mb-4">
                            <button type="button" onClick={this.handleSubmit} className="group relative flex w-full justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
                                Register
                            </button>
                        </div>
                    </form>
                </div>

                <div className="flex min-h-full items-center justify-center px-4">
                    <div className="w-full max-w-lg space-y-8">

                    </div>
                </div>

                <TableComponent data={this.state.data} handleStatusChange={this.handleStatusChange} />
            </>
        )
    }
}